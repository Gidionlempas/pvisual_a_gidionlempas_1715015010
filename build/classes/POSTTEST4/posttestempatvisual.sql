-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 04:04 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posttestempatvisual`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(10) NOT NULL,
  `nama_anggota` varchar(50) NOT NULL,
  `umur` int(3) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `nama_anggota`, `umur`, `pekerjaan`) VALUES
(9, 'dion', 19, 'programer'),
(10, 'wendy', 20, 'dokter'),
(11, 'gerry', 20, 'database desainer'),
(12, 'adit', 22, 'teknisi jaringan'),
(13, 'tegar', 21, 'manager pt.badak'),
(14, 'ucok', 22, 'Pelawak'),
(15, 'Hanie', 20, 'perawat'),
(16, 'aldi laksamana', 19, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nama` varchar(50) NOT NULL,
  `kelamin` varchar(10) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `jabatan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nama`, `kelamin`, `nik`, `jabatan`) VALUES
('gerry', 'Laki-Laki', '123456789', 'Ketua'),
('kun', 'Wanita', '213245', 'Anggota'),
('amos kornelius', 'Pria', '234876', 'Wakil'),
('dika', 'Pria', '3211223', 'Anggota'),
('lutfhi', 'Pria', '3214567', 'Bendahara'),
('melinda', 'Perempuan', '5432167889', 'Sekretaris'),
('santana', 'Wanita', '567890', 'Anggota'),
('rika', 'Perempuan', '987654321', 'Wakil');

-- --------------------------------------------------------

--
-- Table structure for table `simpan_pinjam`
--

CREATE TABLE `simpan_pinjam` (
  `id_sp` int(10) NOT NULL,
  `saldo` varchar(10) NOT NULL,
  `id_anggota` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simpan_pinjam`
--

INSERT INTO `simpan_pinjam` (`id_sp`, `saldo`, `id_anggota`) VALUES
(15, '20000000', 9),
(16, '100000000', 11),
(17, '200000', 10),
(18, '-1000000', 14),
(19, '-100000', 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `simpan_pinjam`
--
ALTER TABLE `simpan_pinjam`
  ADD PRIMARY KEY (`id_sp`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `simpan_pinjam`
--
ALTER TABLE `simpan_pinjam`
  MODIFY `id_sp` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `simpan_pinjam`
--
ALTER TABLE `simpan_pinjam`
  ADD CONSTRAINT `simpan_pinjam_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
